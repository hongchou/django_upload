from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import IMG


# Create your views here.


@csrf_exempt
def uploadImg(request):
    if request.method == 'POST':
        new_img = IMG(
            img=request.FILES.get('img'),
            name=request.FILES.get('img').name
            # name='test.jpg'
        )
        new_img.save()
    return render(request, 'uploadimg.html')


@csrf_exempt
def showImg(request):
    imgs = IMG.objects.all()
    print(str(imgs) + '-------------')
    content = {
        'imgs': imgs,
    }
    for i in imgs:
        print(i.img.url)
    return render(request, 'showimg.html', content)
