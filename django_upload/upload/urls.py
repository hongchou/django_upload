from django.conf.urls import url
from . import views

urlpatterns = [


    url(r'^upload', views.uploadImg),
    url(r'^show', views.showImg),
]
